FROM node

# Copy current directory but keepaway everything in the .dockerignore
COPY . . 

RUN npm install
RUN npx gulp browserify 


EXPOSE 3000

CMD [ "npm", "start" ]
